module.exports = {
  printWidth: 80,
  arrowParens: "avoid",
  semi: false,
  useTabs: true,
  vueIndentScriptAndStyle: true,
  singleQuote: true,
  trailingComma: "es5",
};
