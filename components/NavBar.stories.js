import NavBar from "./NavBar.vue";
import NavBarLink from "./NavBarLink.vue";

export default {
  title: "Navigation/NavBar",
  component: NavBar,
  argTypes: {},
};

const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { NavBar, NavBarLink },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return {
      args,
    };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: `
    <NavBar v-bind="args">
        <template v-slot:tabs >
          <NavBarLink>Projects</NavBarLink>
          <NavBarLink>Experience</NavBarLink>
          <NavBarLink>Curriculum Vitae</NavBarLink>
        </ template>
    </ NavBar>`,
});

export const Primary = Template.bind({});
Primary.args = {
  label: "test Title",
  icon: "test",
};
