import WhoAmI from "./WhoAmI.vue";

export default {
  title: "HomePage/WhoAmI",
  component: WhoAmI,
  argTypes: {
    name: { control: "text" },
    titleBeforeName: { control: "text" },
    titleAfterName: { control: "text" },
    subTitle: { control: "text" },
  },
};

const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { WhoAmI },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return {
      args,
    };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: `
    <div class="h-screen">
        <WhoAmI v-bind="args" />
    </div>
    `,
});

export const Primary = Template.bind({});
Primary.args = {
  name: "Bastiaan Van denabeele",
  titleBeforeName: "Hi, I'm",
  titleAfterName: ".",
  subTitle: "An engineer turned Full stack developer",
};
