import AboutMe from "./AboutMe.vue";

export default {
  title: "HomePage/AboutMe",
  component: AboutMe,
  argTypes: {
    message: { control: "text" },
  },
};

const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { AboutMe },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return {
      args,
    };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: `
    <div class="h-screen">
        <AboutMe v-bind="args" >
            <p>Engineering is in my genes.</p>
            <p>
                I'm driven by solving technical challenged in multiple field. mechanical, software, people, organisation. this has led to ",
            </p>
        </AboutMe>
    </div>
    `,
});

export const Primary = Template.bind({});
