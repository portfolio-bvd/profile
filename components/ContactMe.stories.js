import ContactMe from "./ContactMe.vue";

export default {
  title: "HomePage/ContactMe",
  component: ContactMe,
  argTypes: {},
};

const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { ContactMe },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return {
      args,
    };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: `
    <div class="h-screen">
    <ContactMe v-bind="args" />

    </div>
    `,
});

export const Primary = Template.bind({});
