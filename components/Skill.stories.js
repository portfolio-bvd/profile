import Skill from "./Skill.vue";

export default {
  title: "HomePage/Skill",
  component: Skill,
  argTypes: {
    name: { control: { type: "text" } },
  },
};

const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { Skill },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return {
      args,
    };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<Skill v-bind="args" />',
});

export const Primary = Template.bind({});
Primary.args = {
  name: "Python",
};
