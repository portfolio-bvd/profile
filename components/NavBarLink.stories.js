import NavBarLink from "./NavBarLink.vue";

export default {
  title: "Navigation/NavBarLink",
  component: NavBarLink,
  argTypes: {},
};

const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { NavBarLink },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return {
      args,
    };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<NavBarLink v-bind="args" >NavBarLink</NavBarLink>',
});

export const Primary = Template.bind({});
