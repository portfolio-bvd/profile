import Loading from "./Loading.vue";

export default {
  title: "Utils/Loading",
  component: Loading,
  argTypes: {},
};

const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { Loading },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return {
      args,
    };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: `
    <div class="h-screen">
        <Loading v-bind="args" />
    </div>
    `,
});

export const Primary = Template.bind({});
