import SkillList from "./SkillList.vue";
import Skill from "./Skill.vue";

export default {
  title: "HomePage/SkillList",
  component: SkillList,
  argTypes: {},
};

const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { SkillList, Skill },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return {
      args,
    };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: `
    <div class="h-screen">
        <SkillList v-bind="args" >
            <Skill name="Python">
            <Skill name="Javascript">
        </ SkillList >
    </div>
    `,
});

export const Primary = Template.bind({});
