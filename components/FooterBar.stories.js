import FooterBar from "./FooterBar.vue";

export default {
  title: "Navigation/FooterBar",
  component: FooterBar,
  argTypes: {},
};

const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { FooterBar },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return {
      args,
    };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<FooterBar v-bind="args" />',
});

export const Primary = Template.bind({});
