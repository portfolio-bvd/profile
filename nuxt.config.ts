import { defineNuxtConfig } from "nuxt";

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  ssr: false,
  alias: {
    "@": "./",
  },
  bridge: process.env.NODE_ENV !== "production",
  publicRuntimeConfig: {
    storyBlokVersion: process.env.STORYBLOK_VERSION || "published",
    storyBlokTypeToken: process.env.STORYBLOK_TYPE_TOKEN,
  },
  privateRuntimeConfig: {},
  buildModules: [
    "@nuxtjs/tailwindcss",
    [
      "@storyblok/nuxt",
      {
        accessToken:
          process.env.STORYBLOK_TYPE_TOKEN === "preview"
            ? process.env.STORYBLOK_API_KEY_PREVIEW
            : process.env.STORYBLOK_API_KEY_PUBLIC,
      },
    ],
  ],
  components: {
    global: true,
    dirs: ["~/components/storyblok", "~/components"],
  },
  // https://github.com/storyblok/storyblok-nuxt/issues/81
  build: {
    transpile: ["#app"],
  },
  tailwindcss: {
    cssPath: "~/assets/css/tailwind.css",
    configPath: "tailwind.config.js",
    exposeConfig: false,
    config: {},
    injectPosition: 0,
    viewer: true,
  },
  nitro: {
    prerender: {
      routes: ["/projects", "/experience"],
    },
  },
});
